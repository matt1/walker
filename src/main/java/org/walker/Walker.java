package org.walker;

import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.walker.walk.Walk;

/**
 * Creates a new walker
 * 
 * @author Matt
 *
 */
public class Walker {

	/** Default output stream to use for output */
	PrintStream output = System.out;
	
	/** Default logger */
	private static Logger log  = Logger.getLogger(Walker.class.getName());

	/**
	 * Contains all of the command line options for use with args4j
	 * @author matt
	 *
	 */
	private class CommandOptions {
		
		@Option(name="-help", aliases="-h", usage="Usage instructions")
		private Boolean help = false;
		
		@Option(name="-start", usage="The starting URL")
		private String start;

		@Option(name="-singledomain", usage="Restrict links to the same domain")
		private Boolean singleDomain = Boolean.FALSE;
		
		@Option(name="-depth", usage="The depth that each link should be followed by (default 3)")
		private Integer depth = 3;
		
		@Option(name="-max", usage="The maximum number of links to follow (default 500)")
		private Integer maxLinks = 500;
		
		@Option(name="-regex", usage="Regex to filter out URLs that match")
		private String filter;
		
		@Option(name="-regexInclude", usage="If supplied the filter will include regex matches, otherwise if not provided the regex will filter results out (i.e. exclude)")
		private Boolean filterIsInclude = false;
		
		@Argument
		private List<String> arguments = new ArrayList<String>();


		public String getStart() {
			return start;
		}

		public Integer getDepth() {
			return depth;
		}

		public Integer getMaxLinks() {
			return maxLinks;
		}

		public Boolean getSingleDomain() {
			return singleDomain;
		}
		
		public String getFilter() {
			return this.filter;
		}
		
		public Boolean getIsFilterInclude() {
			return this.filterIsInclude;
		}
		
		public Boolean getHelp() {
			return this.help;
		}
		
	}
	
	/**
	 * Create a new walker and process the arguments
	 * 
	 * @param arguments
	 */
	public static void main(String[] arguments) {
		Walker walker = new Walker();
		walker.init(arguments);
	}
	
	/**
	 * Initialise the application - start by processing the command line arguments
	 * 
	 * @param arguments
	 */
	public void init(String[] arguments) {
		
		// try parsing the command line to see if we got anything
		CommandOptions commandOptions = new CommandOptions();
		CmdLineParser commandParser = new CmdLineParser(commandOptions);

		try {
			commandParser.parseArgument(arguments);							
				
			if (commandOptions.getHelp()) {
				commandParser.printUsage(System.err);
			} else if (null != commandOptions.getStart()) {
				noGui(commandOptions);				
			} else {
				gui(arguments);
			}
			
		} catch (CmdLineException cle) {
			log.info("Bad arguments!  Leave blank to start GUI, or:");
			commandParser.printUsage(System.err);
		}
	}
	
	/**
	 * Kick off the GUI
	 */
	private void gui(String[] arguments) {
		WalkerApplication.launch(WalkerApplication.class, arguments);
	}
	
	/**
	 * Process the input in command line mode
	 * 
	 * @param cmdOpt Options from the command line
	 */
	private void noGui(CommandOptions cmdOpt) {
		
		try {
			Walk w = new Walk(cmdOpt.getStart(), cmdOpt.getDepth(), cmdOpt.getMaxLinks(), 
					cmdOpt.getSingleDomain(), cmdOpt.getFilter(), cmdOpt.getIsFilterInclude());
			
				w.start((String result) -> {				
					System.out.println(result);					
				}, (Integer count) -> {
					// nop for CLI
				});

		} catch (MalformedURLException mfue) {
			log.severe("The URL provided was malformed and could not be used:" + mfue.getMessage());
		}
		
	}
	
	
	/**
	 * Set an output stream for use by the default output
	 * 
	 * @param out
	 */
	public void setOutputStream(PrintStream out) {
		output = out;
	}
	
}
