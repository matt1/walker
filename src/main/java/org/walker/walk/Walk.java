package org.walker.walk;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


/**
 * A new walk;
 * 
 * @author matt
 *
 */
public class Walk {

	/** Default logger */
	private static Logger log  = Logger.getLogger(Walk.class.getName());
	
	private String start;
	
	private Integer currentDepth;
	
	private Integer maxCount;
	
	private Boolean singleDomain;
	
	private Predicate<String> pattern;
	
	private Boolean isFilterInclude;
	
	/** The forkjoin pool used for processing */
	private ForkJoinPool pool;
	
	/** the original domain - used to check URLs when singleDOmain is set */
	private String originalDomain;
	
	/**
	 * Setup a new walk
	 * 
	 * @param start
	 * @param depth
	 * @param max
	 * @param singleDomain
	 * @throws MalformedURLException 
	 */
	public Walk(String start, Integer depth, Integer max, Boolean singleDomain, String pattern,
			boolean isFilterInclude) 
			throws MalformedURLException {
		this.start = start;
		this.currentDepth = depth;
		this.maxCount = max;
		this.singleDomain = singleDomain;
		this.originalDomain = getDomain(start);
		if (pattern != null && !pattern.trim().isEmpty() && !pattern.equals("*")) {
			this.pattern = Pattern.compile(pattern).asPredicate();
		} else {
			this.pattern = null;
		}
		this.isFilterInclude = isFilterInclude;
	}
	
	/**
	 * Gets just the domain from the full URL
	 * @param url
	 * @return
	 * @throws MalformedURLException 
	 */
	private String getDomain(String url) throws MalformedURLException {
		URL u = new URL(url);
		String domain = u.getHost();
		
		return domain;
	}
	
	/**
	 * Start the walk
	 * 
	 */
	public List<String> start() {
		
		List<String> results = new ArrayList<>();
		
		start((String result) -> {results.add(result);},(a)->{});	
				
		results.sort(null);
		return results;
	}
	
	/**
	 * Stop the current walk-in-progress
	 */
	public void stop() {
		if (pool != null && !pool.isShutdown()) {			
			pool.shutdownNow();
		}
	}
	
	/**
	 * Start the walk with an update function for real-time feedback
	 * @param updateFunction
	 */
	public void start(Consumer<String> updateFunction, Consumer<Integer> finishedFunction) {
		
	
		Integer maxThreads =  Runtime.getRuntime().availableProcessors() * 8;
		pool = new ForkJoinPool(maxThreads);
		
		HashMap<String, Boolean> visitedUrls = new HashMap<String, Boolean>();
		AtomicInteger currentCount = new AtomicInteger();
		
		Consumer<?> shutdownCallback = ((a)->{pool.shutdownNow();});
		
		try {
			pool.invoke(
					new WalkWorker(
							pool,
							start, 
							visitedUrls, 
							currentCount, 
							maxCount, 
							currentDepth,							
							this.pattern,
							this.isFilterInclude,
							shutdownCallback,
							updateFunction));
			
		} catch (CancellationException ce) {
			log.warning("Pool was cancelled.");	
		} finally {
			finishedFunction.accept(currentCount.get());
		}
		
		
		List<String> results = new ArrayList<>(visitedUrls.size());
		results.addAll(visitedUrls.keySet());
		

		
		
	}
	
	
	/**
	 * Class to do actual walking
	 *
	 * @author Matt
	 *
	 */
	private class WalkWorker extends RecursiveAction {

		private static final long serialVersionUID = -5431421843620994422L;
		private String startUrl;
		private HashMap<String, Boolean> visitedUrls;
		private AtomicInteger currentCount;
		private Integer maxCount;
		private Integer depth;
		private Consumer<?> shutdownCallback;
		private Consumer<String> updateCallback;
		private Predicate<String> pattern = null;
		private Boolean isFilterInclude;
		private ForkJoinPool pool;

		/**
		 * Create a new walk worker
		 * 
		 * @param start URL to start from
		 * @param shutdown Function to call if we need to abort (e.g. due to limits)
		 * @param pattern RegEx Pattern for filtering
		 */
		public WalkWorker(
				ForkJoinPool pool,
				String start, 
				HashMap<String, Boolean> visited, 
				AtomicInteger count, 
				Integer max, 
				Integer depth, 
				Predicate<String> pattern,
				boolean isFilterInclude,
				Consumer<?> shutdown,
				Consumer<String> update) {
			this.pool = pool;
			this.startUrl = start;
			this.currentCount = count;
			this.maxCount = max;
			this.visitedUrls = visited;
			this.depth = depth;
			this.shutdownCallback = shutdown;
			this.updateCallback = update;
			if (pattern != null) {
				this.pattern = pattern;
			}
			this.isFilterInclude = Walk.this.isFilterInclude;
		}
		
		/**
		 * Check if we should continue or not based 
		 * @return
		 */
		private boolean shouldContinue(String url) {
			Boolean proceed = true;
			Boolean domainOk = true;
			
			// If we're only running on one domain, check if we should continue or not
			if (singleDomain) {						
				try {
					proceed = getDomain(url).equals(originalDomain);
					domainOk = proceed;
				} catch (MalformedURLException mfue) {
					// If the URL is broken, skip it.
					proceed = Boolean.FALSE;
				}
			}
			
			if (domainOk && this.pattern != null) {
				
				if (this.pattern.test(url)) {
					if (isFilterInclude) {
						proceed = Boolean.TRUE;
					} else {
						proceed = Boolean.FALSE;
					}
				} else {
					// No match
					if (isFilterInclude) {
						proceed = Boolean.FALSE;
					} else {
						proceed = Boolean.TRUE;
					}
				}
				
			}
			
			return proceed;
		}
		
		@Override
		protected void compute() {
			try {
				List<RecursiveAction> actions = new ArrayList<RecursiveAction>();
				
				// Do a hack to check if we need to terminate early (otherwise on sites with lots
				// of links, we can have HUGE lists of tasks still processing even after
				// shutdown requests.				
				if (this.pool.isTerminating()) { return; }
				
				Document doc = Jsoup.connect(startUrl).ignoreHttpErrors(true).get();	
				currentCount.incrementAndGet();
				Elements links = doc.select("a[href]");
				
				log.info("Crawling " + startUrl);
				
				// Mark this URL as processed
				visitedUrls.put(startUrl, Boolean.TRUE);
				
				// Update
				updateCallback.accept(startUrl);
				
				links.forEach((link) -> {
					String[] parts = link.absUrl("href").split("#");
					if (parts != null && parts.length > 0) {											
						String url = parts[0];											
						Boolean proceed = shouldContinue(url);					
														
						// Only add a new action if we've not seen this URL before and are good to go
						if (proceed && !visitedUrls.containsKey(url) && depth > 0) {
							
							// Add the URL to the visited list, but mark processing as false
							visitedUrls.put(url, Boolean.FALSE);
							actions.add(new WalkWorker(
									pool,
									url, 
									visitedUrls,
									currentCount,
									maxCount, 
									depth-1, 
									pattern, 
									isFilterInclude,
									shutdownCallback, 
									updateCallback));
						}
					}
										
				});
												
				// kick off all recursive actions if we are at a positive depth and have room
				if (currentCount.get() < maxCount) {
					invokeAll(actions);
				} else {
					shutdownCallback.accept(null);
					log.info("Not invoking any more actions due to hitting limits @depth: " + 
							depth + " & @count: " + currentCount.get() + "/" + maxCount);
				}
				
			} catch (UnsupportedMimeTypeException usmte) {
				log.warning("Mimetype not supported: " + usmte.getMessage());
			} catch (IOException ioe) {
				log.warning("IOException caught during walk: " + ioe.getMessage());
			}
						
		}
		
	}
	
}
