package org.walker.walk;

/**
 * Encapsultes the crawl settings for passing between dialogs & app
 * @author matt
 *
 */
public class CrawlSettings {

	private String startUrl;
	private Boolean singleDomain;
	private int maxResults;
	private int maxDepth;
	private String filterPattern;
	private boolean isFilterInclude;
	
	public CrawlSettings(String startUrl, Boolean singleDomain, int maxResults, int maxDepth, 
			String filterPattern, Boolean isFilterInclude) {
		this.startUrl = startUrl;
		this.singleDomain = singleDomain;
		this.maxResults = Math.abs(maxResults);
		this.maxDepth = Math.abs(maxDepth);
		this.filterPattern = filterPattern;
		this.isFilterInclude = isFilterInclude;
		
		if (maxDepth > 500) {
			maxDepth = 500;
		}
		
		if (maxResults > 100000000) {
			maxResults = 100000000;
		}
	}
	
	public String toString() {
		return startUrl;
	}
	
	public String getStartUrl() {
		return this.startUrl;
	}
	
	public Boolean getSingleDomain() {
		return this.singleDomain;
	}
	
	public int getMaxDepth() {
		return this.maxDepth;
	}
	
	public int getMaxResults() {
		return this.maxResults;
	}
	
	public String getFilterPattern() {
		return this.filterPattern;
	}
	
	public Boolean getIsFilterInclude() {
		return this.isFilterInclude;
	}
	
}
