package org.walker;

import java.io.IOException;
import java.net.URL;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.walker.gui.WalkerGui;

/**
 * Initialises the JavaFX gui
 * 
 * @author matt
 *
 */
public class WalkerApplication extends Application {

	/** Default logger */
	private static Logger log  = Logger.getLogger(WalkerApplication.class.getName());
	
	@Override
	public void start(Stage stage) throws IOException {
		log.info("Starting Walker GUI...");
		URL fxmlUrl = getClass().getResource("/org/walker/gui/WalkerGui.fxml");
		log.info("Loading FXML " + fxmlUrl.toString() );
		FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl);
        Parent root = fxmlLoader.load();
        WalkerGui controller = (WalkerGui) fxmlLoader.getController();
        fxmlLoader.setRoot(root);
        
		
        Scene scene = new Scene(root);		     
        stage.setTitle("Walker");
        stage.setScene(scene);
        stage.show();
        
        controller.setStage(stage);
	}

	public static void main(String[] args) {
		launch(args);
	}
	
}
