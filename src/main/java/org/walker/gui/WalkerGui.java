package org.walker.gui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

import org.controlsfx.dialog.DialogStyle;
import org.controlsfx.dialog.Dialogs;
import org.northheath.dialogs.about.AboutDialog;
import org.northheath.dialogs.help.Help;
import org.northheath.dialogs.help.HelpGui;
import org.walker.gui.dialog.CrawlDialog;
import org.walker.walk.CrawlSettings;
import org.walker.walk.Walk;

public class WalkerGui implements Initializable {

	/** Default logger */
	private static Logger log  = Logger.getLogger(WalkerGui.class.toString());
	
	/** Stage */
	private Stage stage;
	
	/** Walk */
	private Walk walk;
	
	/** Bindings for controls */
	@FXML Parent root;
	
	// File menu
	@FXML private MenuItem newCrawlMenu;
	@FXML private MenuItem save;
	@FXML private MenuItem saveMap;
	@FXML private MenuItem close;
	
	// Help Menu
	@FXML private MenuItem helpContents;
	@FXML private MenuItem about;
	
	// Tool bar
	@FXML private Button saveButton;
	@FXML private Button newCrawl;
	@FXML private Button cancelCrawl;
	@FXML private Button helpButton;
	
	// Results
	@FXML private ListView<String> resultsList;
	private ObservableList<String> resultItems = FXCollections.observableArrayList();
	

	/**
	 * Called by JavaFX when initialisation of this controller is complete
	 */
	public void initialize(URL fxmlFileLocation, ResourceBundle resource) {		
		setupBinding();	
		cancelCrawl.disableProperty().setValue(Boolean.TRUE);
	}
	
	/**
	 * Set the stage
	 */
	public void setStage(Stage stage) {
		this.stage = stage;		
		this.stage.getIcons().add(new Image(getClass().getResource("icon.png").toExternalForm()));
	}
	
	/**
	 * Setup binding to the model
	 */
	private void setupBinding() {

	}
	
	/**
	 * Start a new crawl based on the settings provided
	 * 
	 * @param settings
	 */
	public void startCrawl(CrawlSettings settings ) {
		log.info("Starting new crawl with settings: " + settings.toString());
		try {
			walkStarted();
			// Do some basic checks on the URL first.
			try {
				URL url = new URL(settings.getStartUrl());
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestMethod("HEAD");
				conn.connect();
				conn.disconnect();
				conn = null;
			} catch (MalformedURLException e) {
				walkStopped();
				showWarning("The URL provided was malformed and cannot be crawled.  Please " +
						"check the format of the URL.", "The Starting URL was malformed.");
				
				return;
			} catch (IOException e) {
				walkStopped();
				showWarning("Unable to open the provided start URL which means that the " + 
						"crawl cannot start!\n\nPlease double-check that '" + settings.getStartUrl()
						+ "' is a valid, working URL and try again.", "The Starting URL could not" +
						" be accessed.");
				return;
			}
			
			
			walk = new Walk(settings.getStartUrl(), 
					settings.getMaxDepth(), 
					settings.getMaxResults(), 
					settings.getSingleDomain(), 
					settings.getFilterPattern(),
					settings.getIsFilterInclude());
			

			resultItems.clear();
			resultsList.setItems(resultItems);
			
			Task<Void> crawlTask = new Task<Void>() {
				
				@Override
				protected Void call() throws Exception {
					walk.start((String result) -> {
						Platform.runLater(() -> {
							resultItems.add(result);							
						});
					}, (Integer count) -> {
						Platform.runLater(() -> {
							walkStopped();
						});
					});
					return null;
				}
			};
			
			Thread t = new Thread(crawlTask);
			t.setDaemon(true); 
			t.start();
						
		} catch (MalformedURLException e) {
			showException(e, "The URL provided was malformed and cannot be crawled.  Please " +
					"check the format of the URL.");
		}
	}
	
	/**
	 * Started a new walk
	 */
	private void walkStarted() {
		
		Platform.runLater(() -> {
			newCrawl.disableProperty().setValue(Boolean.TRUE);
			cancelCrawl.disableProperty().setValue(Boolean.FALSE);
			newCrawlMenu.disableProperty().setValue(Boolean.TRUE);

			 this.stage.getScene().setCursor(Cursor.WAIT);
		});
	}
	
	private void walkStopped() {
		Platform.runLater(() -> {
			if (walk != null) {
				walk.stop();
			}
			
			newCrawl.disableProperty().setValue(Boolean.FALSE);
			cancelCrawl.disableProperty().setValue(Boolean.TRUE);
			newCrawlMenu.disableProperty().setValue(Boolean.FALSE);
			
			 this.stage.getScene().setCursor(Cursor.DEFAULT);
		});
	}
	
	/**
	 * New crawl button
	 * 
	 * @param event
	 */
	@FXML protected void handleNewCrawlAction(ActionEvent event) {
		new CrawlDialog(this.stage.getOwner(), this::startCrawl).show();
	}
	
	/**
	 * Stop crawl button
	 * 
	 * @param event
	 */
	@FXML protected void handleStopCrawlAction(ActionEvent event) {
		walkStopped();
	}
	
	/**
	 * Close menu
	 * 
	 * @param event
	 */
	@FXML protected void handleCloseMenuAction(ActionEvent event) {
		walkStopped();
		System.exit(0);
	}
	
	/**
	 * Help oOntents menu
	 * 
	 * @param event
	 */
	@FXML protected void handleHelpMenuAction(ActionEvent event) {
		try {
			HelpGui help = Help.getInstance().show();
			help.loadHelp(getClass().getResource("/help/index.html"));
		} catch (IOException e) {
			showException(e, "Unable to show the help as the application was unable to load the" +
					" help files.");
		}
	}
	
	/**
	 * New crawl menu
	 * 
	 * @param event
	 */
	@FXML protected void handleNewCrawlMenuAction(ActionEvent event) {
		handleNewCrawlAction(event);
	}

	/**
	 * New crawl menu
	 * 
	 * @param event
	 */
	@FXML protected void handleAboutMenuAction(ActionEvent event) {
		new AboutDialog(this.stage.getOwner()).show();
	}
	
	/**
	 * Help toolbar button 
	 * 
	 * @param event
	 */
	@FXML protected void handleHelpButtonAction(ActionEvent event) {
		handleHelpMenuAction(event);
	}
	
	/**
	 * Save toolbar button
	 * 
	 * @param event
	 */
	@FXML protected void handleSaveAction(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save crawl output");
		fileChooser.setInitialFileName("crawl.txt");
		
		ObservableList<ExtensionFilter> filters = FXCollections.observableArrayList(
				new ExtensionFilter("Text Files", "*.txt"),
				new ExtensionFilter("All Files", "*.*")
				);
		fileChooser.getExtensionFilters().addAll(filters);
		File file = fileChooser.showSaveDialog(this.stage);

		StringBuilder builder = new StringBuilder();
		for (String r : resultItems) {
			builder.append(r);
			builder.append("\n");
		}

		try {
			
			FileWriter fooWriter = new FileWriter(file.getAbsolutePath(), false);
			fooWriter.write(builder.toString());
			fooWriter.close();

		} catch (IOException e) {
			showException(e, "Error saving file.  Please check that you have write permissions.");
		}
		
	}
	
	/**
	 * Save site map
	 * 
	 * @param event
	 */
	@FXML protected void handleSaveSiteMapAction(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save sitemap output");
		fileChooser.setInitialFileName("sitemap.xml");
		
		ObservableList<ExtensionFilter> filters = FXCollections.observableArrayList(
				new ExtensionFilter("XML Files", "*.xml"),
				new ExtensionFilter("All Files", "*.*")
				);
		fileChooser.getExtensionFilters().addAll(filters);
		File file = fileChooser.showSaveDialog(this.stage);

		StringBuilder builder = new StringBuilder();
		builder.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		builder.append("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" " + 
				"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " + 
				"xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9 " + 
				"http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n");

		for (String r : resultItems) {
			builder.append("\t<url>\n");
			builder.append("\t\t<loc>");
			builder.append(r);
			builder.append("</loc>\n");			
			builder.append("\t</url>\n");
		}
		
		builder.append("</urlset>");
		try {
			
			FileWriter fooWriter = new FileWriter(file.getAbsolutePath(), false);
			fooWriter.write(builder.toString());
			fooWriter.close();

		} catch (IOException e) {
			showException(e, "Error saving file.  Please check that you have write permissions.");
		}
		
	}
	
	/**
	 * Show a generic exception message
	 * 
	 * @param e
	 * @param message
	 */
	private void showException(Exception e, String message) {
		Dialogs.create()
			.title("Unexpected Error")
			.masthead("An unexpected error was encountered.")
			.style(DialogStyle.NATIVE)
			.message(message)
			.showException(e);
	}
	
	/**
	 * Show a generic exception message
	 * 
	 * @param e
	 * @param message
	 */
	private void showWarning(String message, String title) {
		Dialogs.create()
			.title("Warning")
			.masthead(title)
			.style(DialogStyle.NATIVE)
			.message(message)
			.showWarning();
	}
}
