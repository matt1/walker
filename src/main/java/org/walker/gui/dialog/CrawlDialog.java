package org.walker.gui.dialog;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Window;

import org.controlsfx.control.ButtonBar;
import org.controlsfx.control.ButtonBar.ButtonType;
import org.controlsfx.control.action.AbstractAction;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.DialogStyle;
import org.controlsfx.validation.ValidationResult;
import org.controlsfx.validation.ValidationSupport;
import org.walker.walk.CrawlSettings;


/**
 * Crawl dialog
 * 
 * @author matt
 *
 */
public class CrawlDialog {

	/** Window owner */
	Window owner;
	
	/** Callback to use once the dialog completes */
	Consumer<CrawlSettings> callback;
	
	/** The text field for the start URL */
	final TextField startUrl = new TextField();

	/** The check box for a single domain only */
	final CheckBox singleDomain = new CheckBox();
	
	/** The text field for the maximum number of results */
	final TextField maxResults = new TextField("500");

	/** The text field for the maximum depth */
	final TextField maxDepth = new TextField("5");
	
	/** The text field for the filter pattern */
	final TextField filterPattern = new TextField();
	
	/** Filter type */
	ObservableList<String> options = 
		    FXCollections.observableArrayList(
		        "Include", "Exclude"
		    );
	final ComboBox<String> filterType = new ComboBox<String>(options);
	
	/** Regex for numeric only input check */
	final String numericOnly = "[\\d]+";
	
	/** Validation support */
	ValidationSupport validationSupport = new ValidationSupport();
	
	/** Handler for "ok" */
	final AbstractAction actionStart = new AbstractAction("Start Crawl") {
		{
			ButtonBar.setType(this, ButtonType.OK_DONE);
		}
		
		@Override
		public void handle(ActionEvent event) {
	          Dialog dialog = (Dialog) event.getSource();
	          
	          int maxR = Math.abs(Integer.valueOf(maxResults.getText()));
	          int maxD = Math.abs(Integer.valueOf(maxDepth.getText()));
	          
	          Boolean include = false;
	          if (filterType.getValue().equals("Include")) {
	        	  include = true;
	          }
	          
	          CrawlSettings crawlSettings = new CrawlSettings(
	        		  startUrl.getText(),
	        		  singleDomain.isSelected(),
	        		  maxR,
	        		  maxD,
	        		  filterPattern.getText(),
	        		  include);	          	    
	          Platform.runLater(() -> {
	        	  callback.accept(crawlSettings);
	          });
	          
	          dialog.hide();			
		}
		
	};
	
	/**
	 * Creates a new Crawler dialog
	 * 
	 * @param owner
	 * @param callback
	 */
	public CrawlDialog(Window owner, Consumer<CrawlSettings> callback) {
		this.owner = owner;
		this.callback = callback;
		
		setupValidation();

	}
	
	private void setupValidation() {
		// Setup validators		
		validationSupport.registerValidator(maxDepth,
				(Control c, String value) -> ValidationResult.fromErrorIf(
        				c, 
        				"Maximum depth must be provided and must be numeric.", 
        				(value.trim().isEmpty() || !value.matches(numericOnly))));
		
		validationSupport.registerValidator(maxResults,
				(Control c, String value) -> ValidationResult.fromErrorIf(
        				c, 
        				"Maximum results must be provided and must be numeric.", 
        				(value.trim().isEmpty() || !value.matches(numericOnly))));
		
		validationSupport.registerValidator(startUrl,
				(Control c, String value) -> ValidationResult.fromErrorIf(
        				c, 
        				"Starting URL must be provided", 
        				(urlIsInvalid(value))));

		validationSupport.registerValidator(filterPattern,
				(Control c, String value) -> ValidationResult.fromErrorIf(
        				c, 
        				"Regex pattern is invalid", 
        				( patternInvalid(value) )));
		
		// Mark all as not required to hide the red triangle
		ValidationSupport.setRequired(maxDepth, false);
		ValidationSupport.setRequired(maxResults, false);
		ValidationSupport.setRequired(startUrl, false);
		ValidationSupport.setRequired(filterPattern, false);

	}
	
	/**
	 * Check regex
	 * 
	 * @param pattern
	 * @return
	 */
	private Boolean patternInvalid(String pattern) {
	    Boolean regexInvalid = false;
		try {
	    	if (pattern != null && !pattern.isEmpty()) {
		    	Pattern.compile(filterPattern.getText());
	    	}
	    } catch (PatternSyntaxException pse) {
	    	regexInvalid = true;
	    }
		return regexInvalid;
	}
	
	/**
	 * Check URL
	 * 
	 * @param url
	 * @return
	 */
	private Boolean urlIsInvalid(String url) {
		 Boolean urlInvalid = false;
		 
		 try {
			URL u = new URL(url);
			if (!u.getProtocol().toLowerCase().equals("http") &&
				!u.getProtocol().toLowerCase().equals("https")) {
				urlInvalid = true;
			}
			
			if (u.getHost() == null || u.getHost().isEmpty()) {
				urlInvalid = true;
			}
		} catch (MalformedURLException e) {
			urlInvalid = true;
		}
		 
		 return urlInvalid;
	}
	
	/**
	 * Validate the input
	 */
	 private void validate() {

	     actionStart.disabledProperty().set( 
			urlIsInvalid(startUrl.getText()) ||
			patternInvalid(filterPattern.getText()) ||
			startUrl.getText().trim().isEmpty() ||
			maxDepth.getText().trim().isEmpty() ||
			maxResults.getText().trim().isEmpty() ||
			!maxDepth.getText().matches(numericOnly) ||
			!maxResults.getText().matches(numericOnly)
	     );


	 }

	 /**
	  * Show the dialog
	  * 
	  * @param owner
	  */
	 public void show() {
		 Dialog dialog = new Dialog(owner, "Start a new Crawl",false, DialogStyle.NATIVE);
	       
	     // listen to user input on dialog (to enable / disable the login button)
	     ChangeListener<String> changeListener = new ChangeListener<String>() {
	         public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	             validate();
	         }
	     };
	     startUrl.textProperty().addListener(changeListener);
	     maxDepth.textProperty().addListener(changeListener);
	     maxResults.textProperty().addListener(changeListener);
	     filterPattern.textProperty().addListener(changeListener);
	     
	     startUrl.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				//PopOver popUp = new PopOver(new Label("this is the url"));
				//popUp.show(startUrl);
				
			}
	    	 
	     });
	       
	     // layout a custom GridPane containing the input fields and labels
	     final GridPane content = new GridPane();
	     content.setHgap(10);
	     content.setVgap(10);
	       
	     int row = 0;
	     
	     // starting URL
	     content.add(new Label("Starting Url"), 0, row);
	     content.add(startUrl, 1, row);
	     startUrl.setTooltip(new Tooltip("The URL to start crawling from.\n\nShould start with 'http://' or 'https://'."));
	     GridPane.setHgrow(startUrl, Priority.ALWAYS);

	     // single domain
	     singleDomain.setText("Only crawl links from the some domain?");
	     singleDomain.setTooltip(new Tooltip("Select this option if you want to limit the crawl to the same domain as the starting URL.\n\nSelecting this option will prevent the crawl from following links to external websites."));
	     content.add(singleDomain, 1, ++row);
	     GridPane.setHgrow(singleDomain, Priority.NEVER);
	     
	     // Advanced
	     Label l = new Label("Advanced Options");
	     l.setFont(Font.font("sans-serif", FontWeight.BOLD, 12));
	     content.add(l, 0, ++row);
	     
	     // filter pattern
	     filterType.setValue("Include");
	     filterType.setPrefWidth(110);
	     content.add(new Label("URL Filter RegEx"), 0, ++row);
	     final HBox filterBox = new HBox(8);
	     filterBox.getChildren().addAll(filterPattern, filterType);
	     content.add(filterBox, 1, row);
	     filterPattern.setTooltip(new Tooltip("The regular expression used to filter URLs.  If the URL does not match this expression it will included or excluded.\n\nLeave blank if you do not want to filter URLs."));
	     filterType.setTooltip(new Tooltip("Including will mean that only URLs that match the regular expression will be crawled.\nExcluding means that URL that match the regular expression will be excluded."));
	     GridPane.setHgrow(filterPattern, Priority.ALWAYS);
	     
	     // max results
	     content.add(new Label("Maximum Results"), 0, ++row);
	     content.add(maxResults, 1, row);
	     maxResults.setAlignment(Pos.CENTER);
	     maxResults.setTooltip(new Tooltip("The number of links that should be crawled before stopping.\n\nMust be a numeric value."));
	     GridPane.setHgrow(maxResults, Priority.NEVER);
	     
	     // max threads
	     content.add(new Label("Maximum Depth"), 0, ++row);
	     content.add(maxDepth, 1, row);
	     maxDepth.setAlignment(Pos.CENTER);
	     maxDepth.setTooltip(new Tooltip("The maximum number of steps away from the start URL that the crawl will visit.\nThis is not a limit on the total number of links to crawl - it is a limit on how 'far away' the crawler will go from the start URL. \n\nMust be a numeric value."));
	     GridPane.setHgrow(maxDepth, Priority.NEVER);
	     
	     // create the dialog with a custom graphic and the gridpane above as the
	     // main content region
	     dialog.setResizable(false);
	     dialog.setIconifiable(false);
	     dialog.setContent(content);
	     dialog.getActions().addAll(actionStart, Dialog.Actions.CANCEL);
	     validate();
	       
	     // Focus start URL immediately
	     Platform.runLater(() -> {
	     	startUrl.requestFocus();
	     });

	     
	     dialog.show();
	 }
	 
	 
	
}

