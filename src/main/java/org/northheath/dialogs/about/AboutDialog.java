package org.northheath.dialogs.about;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Window;

import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.DialogStyle;


/**
 * About dialog
 * 
 * @author matt
 *
 */
public class AboutDialog {

	/** Window owner */
	Window owner;

	/**
	 * Creates a new about dialog
	 * 
	 * @param owner
	 * @param callback
	 */
	public AboutDialog(Window owner) {
		this.owner = owner;
	}
	
	
	 /**
	  * Show the dialog
	  * 
	  * @param owner
	  */
	 public void show() {
		 Dialog dialog = new Dialog(owner, "About",false, DialogStyle.NATIVE);
	       
	    
	     // layout a custom GridPane containing the input fields and labels
	     final GridPane content = new GridPane();
	     content.setHgap(5);
	     content.setVgap(5);
	       
	     int row = 0;
	     

	     Label l = new Label("Walker");
	     l.setFont(Font.font("sans-serif", FontWeight.BOLD, 20));
	     l.setAlignment(Pos.CENTER);
	     content.add(l, 0, ++row);
	     GridPane.setHgrow(l, Priority.ALWAYS);
	     
	     setLabel(++row, "Copyright North Heath Software 2014", content);
	     row += 3;
	     
	     setLabel(++row, "This software uses software developed by open source projects:", content);
	     setLabel(++row, " - Args4j", content);
	     setLabel(++row, " - Apache Commons IO & Collections", content);
	     setLabel(++row, " - ControlsFX", content);
	     setLabel(++row, " - JSoup", content);
	     setLabel(++row, "License information for these software libraries be found in NOTICES.txt"
	    		 ,content);
	   
	     // create the dialog with a custom graphic and the gridpane above as the
	     // main content region
	     dialog.setResizable(false);
	     dialog.setIconifiable(false);
	     dialog.setContent(content);
	     dialog.getActions().addAll(Dialog.Actions.CLOSE);

	     
	     dialog.show();
	 }
	 
	 private void setLabel(int row, String message, GridPane content) {
	     Label l = new Label(message);
	     l.setAlignment(Pos.CENTER);
	     content.add(l, 0, row);
	     GridPane.setHgrow(l, Priority.ALWAYS);
	 }
	 
	
}

