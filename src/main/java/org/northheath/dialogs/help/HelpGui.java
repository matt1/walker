package org.northheath.dialogs.help;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * Controller class for help GUI
 * 
 * @author Matt
 *
 */
public class HelpGui implements Initializable {
	
	/** Stage */
	private Stage stage;
	
	/** Bindings for controls */
	@FXML Parent root;
	@FXML private WebView html;
	
	/** Check if we have already loaded the help or not */
	private Boolean isLoaded = false;
	

	/**
	 * Called by JavaFX when initialisation of this controller is complete
	 */
	public void initialize(URL fxmlFileLocation, ResourceBundle resource) {		
		setupBinding();					
	}
		
	/**
	 * Set the stage and initialise Drag & drop
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
		this.stage.getIcons().add(new Image(getClass().getResource("help.png").toExternalForm()));
		
	}
	
	/**
	 * Setup binding to the model
	 */
	private void setupBinding() {

	}
	
	/**
	 * Reload a new page into the help window
	 */
	public void reload(URL resource) {
		isLoaded = false;
		loadHelp(resource);
	}
	
	/**
	 * Load a help file into the browser
	 * 
	 * @param resource
	 */
	public void loadHelp(URL resource) {
		if (!isLoaded) {
			isLoaded = true;
			html.getEngine().load(resource.toExternalForm());
		}
	}
}
