package org.northheath.dialogs.help;

import java.io.IOException;
import java.util.logging.Logger;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Help {

	/** Default logger */
	private static Logger log  = Logger.getLogger(Help.class.getName());
	
	/** Static instance */
	private static Help instance = new Help();
	
	/** Keep track of if we have already initialised or not */
	private Stage stage;
	
	/** The controller for the gui */
	private HelpGui controller;
	
	public static Help getInstance() {
		return instance;
	}
	
	public HelpGui show() throws IOException {
		log.info("Starting Help...");
		
		if (stage == null) {
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/org/northheath/dialogs/help/HelpWindow.fxml"));
	        Parent root = fxmlLoader.load();
	        controller = (HelpGui) fxmlLoader.getController();
	        fxmlLoader.setRoot(root);
	        		
	        Scene scene = new Scene(root);		
	        stage = new Stage();
	        stage.setTitle("Help");
	        stage.setScene(scene);
		}
        stage.show();
        stage.toFront();
        
        controller.setStage(stage);
        
        return controller;
	}
	
}
