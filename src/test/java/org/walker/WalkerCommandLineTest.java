package org.walker;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class WalkerCommandLineTest {

	PrintStream out;
	ByteArrayOutputStream bytes;
	Walker walker;
	
	
	@Before
	public void setup() {
		
		bytes = new ByteArrayOutputStream();
		out = new PrintStream(bytes);
		walker = new Walker();
		walker.setOutputStream(out);
	}
	
	@After
	public void cleanup() throws IOException {
		out.close();
	}
	/**
	 * Used for command line tests
	 * @return
	 */
	private String out() {
		return new String(bytes.toByteArray()).trim();
	}
	
	@Test
	public void test_cli() throws URISyntaxException {
				
		String[] args = {"-start", "http://www.example.com", "-max", "1"};
		walker.init(args);	
		
		assertEquals("http://www.example.com\nhttp://www.iana.org/domains/example", out());
		
	}
	
	@Test
	public void test_cliSingleDomain() throws URISyntaxException {
				
		String[] args = {"-start", "http://www.example.com", "-max", "1", "-singledomain"};
		walker.init(args);	
		
		assertEquals("http://www.example.com", out());
		
	}
	
	@Test
	public void test_clRegExExclude() throws URISyntaxException {
				
		String[] args = {"-start", "http://www.example.com", "-max", "5", "-regex", "iana\\.org"};
		walker.init(args);	
		
		assertEquals("http://www.example.com", out());
		
	}
	
	@Test
	public void test_clRegExInclude() throws URISyntaxException {
				
		String[] args = {"-start", "http://www.iana.org/domains/reserved", "-max", "50", "-regex", "iana\\.org/about/excellence", "-regexInclude"};
		walker.init(args);	
		
		assertEquals("http://www.iana.org/about/excellence\nhttp://www.iana.org/domains/reserved", out());
		
	}

	
}
